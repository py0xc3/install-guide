
:experimental:

[[sect-installation-gui-installation-source]]
=== Installation Source

The `Installation Source` screen allows you to specify a location (local or on the network) from which packages will be downloaded and installed on your system. This screen will be configured automatically in most cases, but you can change your settings or add additional sources.

[NOTE]
====

Normally, when you first enter the `Installation Summary` screen, the installer will attempt to configure an installation source based on the type of media you used to boot. The full {PRODUCT} Server DVD will configure the source as local media, the netinst ISO image will configure the closest network mirror, etc. This process takes some time, especially if the default source is a network mirror. If you plan to use a custom installation source, use the [option]#inst.askmethod# boot option to skip the initial configuration; this will allow you to enter this screen immediately. See xref:advanced/Boot_Options.adoc#sect-boot-options-sources[Specifying the Installation Source] for information about boot options.

====

.Installation Source

image::anaconda/SourceSpoke.png[The Installation Source screen, configured to download packages to be installed from the closest network mirror. An option to use a locally available ISO file is displayed above.]

The following options are available. Note that not all of them may be displayed.

Auto-detected installation media::  This is the option selected by default if you started the installer from media containing an installation source, such as a live DVD. No additional configuration is necessary. You can click the `Verify` button check the media integrity.

ISO file::  This option will appear if the installation program detected a partitioned hard drive with mountable file systems during boot. Select this option, click the btn:[Choose an ISO] button, and browse to the installation ISO file's location on your system. You can click the `Verify` button to check the file's integrity.

On the network::  Use this option to download packages to be installed from a network location instead of local media. This is the default selection on network installation media.
+
In most cases, the `Closest mirror` option available from the protocol selection drop-down menu is preferable. If this option is selected, packages for your system will be downloaded from the most suitable location (mirror).
+
To manually configure a network-based installation source, use the drop-down menu to specify the protocol to be used when downloading packages. This setting depends on the server you want to use. Then, type the server address (without the protocol) into the address field. If you choose NFS, a second input field will appear where you can specify custom `NFS mount options`.
+
[IMPORTANT]
====

When selecting an NFS installation source, you must specify the address with a colon (`:`) character separating the host name from the path. For example:

[subs="quotes, macros"]
----
`pass:attributes[{blank}]_server.example.com_:pass:attributes[{blank}]_/path/to/directory_pass:attributes[{blank}]`
----

====
+
To configure a proxy for an HTTP or HTTPS source, click the btn:[Proxy setup] button. Check `Enable HTTP proxy` and type the URL into the `Proxy URL` box. If the proxy server requires authentication, check `Use Authentication` and enter your user name and password. Click btn:[OK] to finish the configuration.
+
If your HTTP or HTTPS URL refers to a repository mirror list, mark the check box under the address field.

You can also specify additional repositories in the `Additional repositories` section to gain access to more installation environments and software add-ons. All environments and add-ons will be available for selection in xref:install/Installing_Using_Anaconda.adoc#sect-installation-gui-software-selection[Software Selection] once you finish configuring the sources.

To add a repository, click the btn:[+] button. To delete a repository, select one in the list and click the btn:[-] button. Click the arrow icon to revert to the previous list of repositories, i.e. to replace current entries with those that were present at the time you entered the `Installation Source` screen. To activate or deactivate a repository, click the check box in the `Enabled` column at each entry in the list.

You can name your additional repository and configure it the same way as the primary repository on the network using the input fields on the right side of the section.

Once you have selected your installation source, click `Done` in the top left corner to return to xref:install/Installing_Using_Anaconda.adoc#sect-installation-gui-installation-summary[Installation Summary].
