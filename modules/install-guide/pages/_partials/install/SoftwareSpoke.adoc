
:experimental:

[[sect-installation-gui-software-selection]]
=== Software Selection

The `Software Selection` screen allows you to choose a _Base Environment_ and _Add-ons_. These options control which software packages will be installed on your system during the installation process.

This screen is only available if xref:install/Installing_Using_Anaconda.adoc#sect-installation-gui-installation-source[Installation Source] is properly configured and only after the installer has downloaded package metadata from the source.

[NOTE]
====

It is not possible to select specific packages during a manual installation. You can only select pre-defined environments and add-ons. If you need to control exactly which packages are installed, you must use a Kickstart file and define the packages in the [command]#%packages# section. See xref:advanced/Kickstart_Installations.adoc#chap-kickstart-installations[Automating the Installation with Kickstart] for information about Kickstart installations.

====

The availability of environments and add-ons depends on your installation source. By default, the selection depends on the installation media you used to start the installation; Fedora{nbsp}Server installation image will have different environments and add-ons available for selection than, for example, the Fedora{nbsp}Cloud image. You can change this by configuring a different installation source containing different environments.

.Software Selection

image::anaconda/SoftwareSpoke.png[The Software Selection screen. On the left side, one environment (Fedora Workstation) is selected; the right side displays several optional add-ons (such as LibreOffice and Books and Guides) selected in addition to it.]

To configure your software selection, first choose an environment on the left side of the screen. Only one environment can be chosen, even if more are available. Then, on the right side of the screen, select one or more add-ons which you want to install by marking the check boxes next to each add-on.

The list of add-ons is divided into two parts by a horizontal line. Add-ons above this line are defined as part of your chosen environment; if you select a different environment, the add-ons available here will change. The add-ons displayed below the separator are not specific to your chosen environment.

Environments and add-ons are defined using a `comps.xml` file in your installation source (for example, in the `repodata/` directory on the full Fedora{nbsp}Server installation DVD). Review this file to see exactly which packages will be installed as part of a certain environment or add-on. For more information about the `comps.xml` file, see xref:appendixes/Kickstart_Syntax_Reference.adoc#sect-kickstart-packages[%packages (required) - Package Selection].

After you finish configuring your software selection, click `Done` in the top left corner to return to xref:install/Installing_Using_Anaconda.adoc#sect-installation-gui-installation-summary[Installation Summary].
