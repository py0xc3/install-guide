:experimental:
include::{partialsdir}/entities.adoc[]

[[chap-downloading-fedora]]
= Downloading Fedora
//TEST - DO NOT MERGE THIS !!!
.Fedora Editions
Fedora provides three primary Editions tailored for some specific use cases. link:++https://getfedora.org++[] offers Fedora Cloud for scalable infrastructure, Fedora Server for organizational infrastructure, and Fedora Workstation for the developer and desktop user.

For alternative desktop environments or media built for more niche purposes, check out link:++https://spins.fedoraproject.org++[Fedora Spins].

Each of these downloads provides a different set of default packages, but you can add to your system after the initial installation to customize it for your needs. The installation process is the same for all spins and Editions, so you can use this guide for any choice you make.

.Which Architecture Is My Computer?
Most modern systems are 64{nbsp}bit x86 architecture. If your computer was manufactured after 2007, or you aren't sure, you probably have a `x86_64` system.

Changing a Fedora installation from one architecture to another is not supported. Use the following table to determine the architecture of your computer according to the type of processor. Consult your manufacturer's documentation for details on your processor, or resources such as link:++https://ark.intel.com/++[] or link:++https://products.amd.com/++[], if necessary.

[[List-Processor_and_Architecture_Types]]
.Processor and architecture types

[options="header"]
|===
|Processor manufacturer and model|Architecture type for Fedora
|some Intel Atom, Core series, Pentium 4, and recent vintage Xeon; AMD Athlon, Duron, some Semprons; and older; VIA C3, C7|`i386`
|some Intel Atom, Core 2 series, Core i series and Xeon;
						AMD: Athlon 64, Athlon II, Sempron64, Phenom series, Fusion series, Bulldozer series and Opteron; Apple MacBook, MacBook Pro, and MacBook Air|`x86_64`
|Some newer Arm SBCs, SBSA and SystemReady Arm based machines|`aarch64`
|Some older 32-bit Arm SBCs|`armhfp`
|===

.Media Types
Several media types are available. Choose the one that best suits your requirements.

Live Image::  Live images allow you to preview Fedora before installing it. Instead of booting directly into the installer, a live image loads the same environment you'll get after installation. Fedora Workstation and Fedora Spins are live images.
+
Use a live image to install your favorite system, test Fedora on new hardware, troubleshoot, or share with friends.

DVD Image::  DVD images boot directly into the installation environment, and allow you to choose from a variety of packages that are provided with it. In Fedora 21, the DVD option is only available in the *Fedora Server* Edition.
+
Use the Fedora Server DVD image when you want customized Fedora Server installations using an offline installation source.

netinstall Image::  The netinstall image boots directly into the installation environment, and uses the online Fedora package repositories as the installation source. With a netinstall image, you can install any Fedora Edition or select a wide variety of packages to create a customized installation of Fedora.
+
Compared to Fedora Workstation DVD image with Fedora Workstation installation: the netinstall images do not hide the GRUB2 menu.
+
The link:++https://alt.fedoraproject.org++[netinstall images] have different default settings:
+
* Fedora Server:
** presets Fedora Server Edition
** LVM with XFS file system configuration
** server firewall configuration
** 15 GiB hard disk space assigned to /; remaining free space left as unpartitioned LVM space
* Everything:
** presets Fedora Custom Operating System
** BTRFS file system configuration
** client firewall configuration
** hard disk space assignment does not leave unpartitioned free space

ARM images::  For many ARM systems, Fedora provides preconfigured filesystem images. Write the image to removable media and boot directly into a Fedora installation that's ready to use.
+
ARM devices often require special setup procedures that aren't covered in this guide. Start learning about Fedora ARM at link:++https://fedoraproject.org/wiki/Architectures/ARM++[]

Cloud Images::  Fedora Cloud images are preconfigured filesystem images with very few packages installed by default. They include special tools for interacting with cloud platforms, and are not intended to be used outside of cloud environments.
+
Fedora Cloud comes in several varieties. The Fedora Cloud Base image is a minimal base for cloud deployments. The Fedora Cloud Atomic image is a Docker container host that uses link:++https://www.projectatomic.io/++[Project Atomic] technology for updates. A Docker base image for Fedora is also available.
+
Cloud images are preconfigured and do not require installation as described in this guide. Get started using Fedora Cloud at link:++https://fedoraproject.org/wiki/Cloud++[]

Boot Images::  The tiny images at link:++https://boot.fedoraproject.org/++[] are written to CDs, USB drives, or even floppy disks. The BFO image loads installation media from Fedora's servers and directly loads an installation environment, like the netinstall ISO.
+
BFO images work like PXE deployments, without having to set up a server.
